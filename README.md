# Herder

A gem that groups you stuff on multiple levels

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'herder'
```

And then execute:

```sh
$ bundle
```

Or install it yourself as:

```sh
$ gem install herder
```

## Usage

Just send your list of objects to group and a list of the groups
and you will get a hash back with your objects grouped.

```ruby
Sheep < Struct.new(:name, :age, :color)

sheep = [
  Sheep.new("Arne", 3, "white"),
  Sheep.new("Bertil", 1, "white"),
  Sheep.new("Ceasar", 3, "black"),
]

Herder.group sheep, :age
=> { 1 => ["Bertil"], 3 => ["Arne", "Ceasar"] }

Herder.group sheep, [:color, :age]
=> { "black" => { 3 => ["Ceasar"] }, "white" => { 1 => ["Bertil"], 3 => ["Arne"] } }
```

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
