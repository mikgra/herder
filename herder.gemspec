# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'herder/version'

Gem::Specification.new do |spec|
  spec.name          = "herder"
  spec.version       = Herder::VERSION
  spec.authors       = ["Mikael Granholm"]
  spec.email         = ["mikael@xtreme.se"]
  spec.description   = %q{A gem that helps you herd your objects when they are all over the place!}
  spec.summary       = %q{A library for gropuing objects on multiple levels}
  spec.homepage      = "http://www.xtreme.se"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec", "2.13.0"
end
