require 'spec_helper'
require 'herder'

Sheep = Struct.new(:name, :age, :color)

describe Herder do
  describe ".group" do
    let(:arne) { Sheep.new("Arne", 3, "white") }
    let(:bertil) { Sheep.new("Bertil", 1, "white") }
    let(:ceasar) { Sheep.new("Ceasar", 3, "black") }
    let(:sheep) { [arne, bertil, ceasar] }
    subject { Herder.group sheep, groups }
    
    context "when grouping on a single attribute" do
      let(:groups) { :age }
      
      it { should == { 1 => [bertil], 3 => [arne, ceasar] } }
    end
    
    context "when grouping on two attributes" do
      let(:groups) { [:color, :age] }
      
      it { should == { "black" => { 3 => [ceasar] }, "white" => { 1 => [bertil], 3 => [arne] } } }
    end
    
    context "when grouping on three attributes" do
      let(:groups) { [:age, :color, :name] }
      
      it { should == { 1 => { "white" => { "Bertil" => [bertil] } }, 3 => { "black" => { "Ceasar" => [ceasar] }, "white" => { "Arne" => [arne] } } } }
    end
  end
end