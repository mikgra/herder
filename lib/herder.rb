require "herder/version"

module Herder
  def self.group(objects, attributes)
    attributes = [attributes] unless attributes.is_a?(Array)
    Grouper.new(objects, attributes).group
  end
  
  class Grouper
    attr_reader :objects, :attributes, :groups
    
    def initialize(objects, attributes)
      @objects = objects
      @attributes = attributes
      @groups = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) } 
    end
    
    def group
      objects.each do |o|
        add_to_group(o)
      end
      
      groups
    end
    
    private
    
    def add_to_group(object)
      hash = groups
      attributes[0...-1].each do |attribute|
        hash = hash[object.send(attribute)]
      end
      last_key = object.send(attributes.last)
      hash[last_key] = [] unless hash[last_key].is_a?(Array)
      hash[last_key] << object
    end
  end
end
